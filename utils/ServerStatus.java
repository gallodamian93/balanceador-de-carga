package utils;

public class ServerStatus {
	String ip;
	int port;
	int connected;
	boolean active;
	
	public ServerStatus(String ip, int port) {
		super();
		this.ip = ip;
		this.port = port;
		this.connected = 0;
		this.active = false;
	}
	
	public ServerStatus() {
		super();
		this.ip = null;
		this.port = -1;
		this.connected = -1;
		this.active = false;
	}
	
	public String getSocket(){
		return this.ip + ":" + String.valueOf(this.port);
	}
	
	//getters & setters
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public int getConnected() {
		return connected;
	}
	public void setConnected(int conectados) {
		this.connected = conectados;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}

	
	
	
}
