package balanceador;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import utils.ServerStatus;

public class BalanceadorManager implements Runnable{
	ServerStatus server;
	Balanceador balanceador;
	
	public BalanceadorManager(ServerStatus server, Balanceador balanceador){
		this.server = server;
		this.balanceador = balanceador;
	}
	
	@Override
	public void run() {
		int connected;
		try {
			//inicio conexion con el servidor
			if(send("UP").equals("OK")){
				this.server.setActive(true);
				System.out.println("[Balanceador] Servidor levantado: " + this.server.getSocket());
			}
			
			//me quedo haciendo pulling y actualizando el estado del server
			while(this.server.isActive()){
				//averiguo cuantos clientes tiene conectado
				connected = Integer.parseInt(send("CONNECTED"));
				
				//si es distinto a la cantidad que yo tengo actualizo
				if(Integer.compare(connected, server.getConnected()) != 0){
					server.setConnected(connected);
					//le avisa al balanceador que cambio
					balanceador.balancear();
					System.out.println("[Balanceador] Actualizacion del servidor " + this.server.getSocket() + ": " + "CONNECTED=" + connected);
				}
				//pongo el hilo a dormir 10 segundos
				Thread.sleep(10000);
			}
			
			//salio del while por lo tanto dejo de estar activo
			//debo darlo de baja
			if(send("DOWN").equals("OK")){
				System.out.println("[Balanceador] Servidor dado de baja: " + this.server.getSocket());
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	private String send(String peticion){
		String ip = server.getIp();
		int port = server.getPort();
		String respuesta = "ERROR";
		try {
			Socket s = new Socket(ip, port);
			BufferedReader input = new BufferedReader (new InputStreamReader (s.getInputStream()));
			PrintWriter output = new PrintWriter (new OutputStreamWriter (s.getOutputStream()),true);
			output.println(peticion);
			System.out.println("[Balanceador] Mensaje enviado(" + peticion +") a " + ip + ":" + port);
			respuesta = input.readLine();
			
			input.close();
			output.close();
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return respuesta;
	}
}
