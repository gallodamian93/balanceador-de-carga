package balanceador;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import servidor.Servidor;
import utils.ServerStatus;

public class Balanceador {
	//ArrayList<Thread> levantados;
	ArrayList<ServerStatus> servers;
	int myPort;
	ServerStatus selected; //server mas libre de la lista
	int max; //si conectados/servers > max => levanta server
	int min; //si conectados/servers < min => baja server
	
	public Balanceador(ArrayList<ServerStatus> servers, int myPort, int min, int max) {
		super();
		this.myPort = myPort;
		this.max = max;
		this.min = min;
		this.servers = servers;
	}
	
	public void iniciar() throws InterruptedException{
		while(true){
			try {
				this.balancear();
				ServerSocket ss = new ServerSocket(myPort);
				System.out.println("[Balanceador] Iniciado en puerto " + myPort);
				while (true){
					Socket s = ss.accept();
					//entro un cliente
					System.out.println("[Balanceador] Cliente conectado");
					
					//BufferedReader input = new BufferedReader (new InputStreamReader (s.getInputStream()));
					PrintWriter output = new PrintWriter (new OutputStreamWriter (s.getOutputStream()),true);
					
					//devuelvo socket del servidor al que debe conectarse
					String respuesta = selected.getSocket();
					output.println(respuesta);
					System.out.println("[Balanceador] Respondi: " + respuesta);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	/*
	 * Esta funcion hace 2 cosas:
	 * 1. Decide si debe levantar/bajar un servidor y si asi lo requiere lo hace
	 * 2. Setea el selected
	 */
	public void balancear() throws InterruptedException{
		//averiguo cantidad total de conectados
		int up = 0;
		int connected = 0;
		for (ServerStatus server : servers) {
			if (server.isActive()) {
				connected += server.getConnected();
				up++;
			}
		}
		
		//verifico si hay que levantar o dar de baja un servidor
		boolean upOrDown = false;
		//levanto servidor si no hay ninguno levantado
		if (up == 0){
			this.levantar();
			upOrDown = true;
		} else{
			//me fijo si debo levantar o bajar servidor
			if (connected/up > this.max) {
				//sobrecarga
				this.levantar();
				upOrDown = true;
			} else if (connected/up < this.min) {
				//baja carga
				if(connected != 0){
					this.bajar();
					upOrDown = true;
				}
			}
		}
		//si se levanto o bajo un servidor doy tiempo a que se actualice el estado
		if(upOrDown){
			Thread.sleep(500);
		}
		
		//selecciono servidor con menos carga
		ServerStatus selected = new ServerStatus();
		for (ServerStatus server : servers) {
			if(server.isActive()){
				if ((Integer.compare(server.getConnected(), selected.getConnected()) < 0) || (selected.getConnected() == -1)) {
					selected = server;
				}
			}
		}
		
		//seteo selected
		this.setSelected(selected);
	}
	
	public void bajar(){
		ServerStatus activo = null;
		for (ServerStatus server : servers) {
			if(server.isActive()){
				//encontre uno activo
				activo = server;
				break;
			}
		}
		if (activo != null) {
			//encontro activo
			//le cambio el estado, el thread asociado muere solo
			activo.setActive(false);
		}
	}
	
	public void levantar() throws InterruptedException{
		ServerStatus libre = null;
		for (ServerStatus server : servers) {
			if(!server.isActive()){
				//encontre uno libre
				libre = server;
				break;
			}
		}
		if (libre != null) {
			//encontro libre
			//levanto thread que se encarga de establecer conexion y actualizar estado de server en "active"
			BalanceadorManager bm = new BalanceadorManager(libre, this);
			Thread bmThread = new Thread(bm);
			bmThread.start();
		}
	}
	
	public void setSelected(ServerStatus selected){
		this.selected = selected;
		System.out.println("[Balanceador] Cambi� el servidor a conectarse: " + selected.getSocket());
	}
	
	public static void main(String[] args) throws InterruptedException {
		//configuracion del balanceador
		int myPort = 10000;
		int min = 2;
		int max = 5;
		//lista de servidores disponibles para levantar/bajar
		ArrayList<ServerStatus> servers = new ArrayList<ServerStatus>();
		ServerStatus sv1 = new ServerStatus("127.0.0.1", 10001);
		ServerStatus sv2 = new ServerStatus("127.0.0.1", 10002);
		servers.add(sv1);
		servers.add(sv2);
		
		//inicializo
		Balanceador bal = new Balanceador(servers, myPort, min, max);
		bal.iniciar();
	}
}
