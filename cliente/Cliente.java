package cliente;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import servidor.Servidor;
import utils.ServerStatus;

public class Cliente {
	ServerStatus server; //balanceador
	
	public Cliente(ServerStatus server){
		this.server = server;
	}
	
	public void connect() throws InterruptedException{
		//pido ip del servidor al balanceador
		String socketServer = this.send("");
		if(socketServer != null){
			//tengo socket del server, ahora solicito conectarme
			if(this.send("CONNECT", socketServer).equals("OK")){
				//conectado!
				System.out.println("[Cliente] Conectado al servidor");
				//simulo tiempo de trabajo y me desconecto
				Thread.sleep(10000);
				if(this.send("DISCONNECT", socketServer).equals("OK")){
					System.out.println("[Cliente] Desconectado del servidor");
				}
			}
		}
	}
	
	
	private String send(String peticion){
		String ip = server.getIp();
		int port = server.getPort();
		String respuesta = "ERROR";
		try {
			Socket s = new Socket(ip, port);
			BufferedReader input = new BufferedReader (new InputStreamReader (s.getInputStream()));
			PrintWriter output = new PrintWriter (new OutputStreamWriter (s.getOutputStream()),true);
			output.println(peticion);
			System.out.println("[Cliente] Mensaje enviado(" + peticion +") a " + ip + ":" + port);
			respuesta = input.readLine();
			
			input.close();
			output.close();
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return respuesta;
	}
	
	private String send(String peticion, String socketServer){
		String ip = socketServer.split(":")[0];
		int port = Integer.parseInt(socketServer.split(":")[1]);
		String respuesta = "ERROR";
		try {
			Socket s = new Socket(ip, port);
			BufferedReader input = new BufferedReader (new InputStreamReader (s.getInputStream()));
			PrintWriter output = new PrintWriter (new OutputStreamWriter (s.getOutputStream()),true);
			output.println(peticion);
			System.out.println("[Cliente] Mensaje enviado(" + peticion +") a " + ip + ":" + port);
			respuesta = input.readLine();
			
			input.close();
			output.close();
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return respuesta;
	}
	
	public static void main(String[] args) throws InterruptedException {
		ServerStatus server = new ServerStatus("127.0.0.1", 10000);
		Cliente cli = new Cliente(server);
		cli.connect();
	}

	
}
