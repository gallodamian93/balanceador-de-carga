## Sobre el proyecto

Red flexible y elástica de nodos (servicios) que se adapta (crece /
decrece) dependiendo de la carga de trabajo de la misma. El esquema es el de un
balanceador de carga y nodos detrás que atienden los pedidos. Se implementa:

- Simulación de carga de nodos. Creación dinámica de conexiones de clientes y
  pedidos de atención al servicio publicado.
- Protocolo de sensado para carga general del sistema.
- Creación, puesta en funcionamiento de los servicios nuevos, y remoción de ellos
  cuando no sean más necesarios.

## Instrucciones

1. Levantar dos instancias de Servidor.java (en puertos 10001 y 10002)
2. Levantar Balanceador.java
3. Levantar muchas instancias de Clientes: la rutina de los clientes es pedirle al balanceador donde conectarse y cuando este contesta se conectan, luego de unos segundos se desconectan. El balanceador hace pulling en los servidores para saber el estado de carga de cada uno (clientes conectados) y en base a esta informacion selecciona al servidor mas liberado para que cuando un cliente se conecte le mande la ip de este. En la consola del balanceador se va a observar como cambia de servidor a la hora de dar una respuesta a los clientes.