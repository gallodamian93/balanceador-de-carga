package servidor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import utils.ServerStatus;

public class Servidor {
	ServerStatus myConf;
	int clientes;
	
	public Servidor(ServerStatus myConf, int clientes) {
		super();
		this.myConf = myConf;
		this.clientes = clientes;
	}

	public void iniciar(){
		while(true){
			try {
				ServerSocket ss = new ServerSocket(myConf.getPort());
				System.out.println("[Server] Iniciado en puerto " + myConf.getPort());
				while (true){
					Socket s = ss.accept();
					//entro un cliente (en este contexto se entiende por cliente tanto a los clientes que solicitan un servicio como al balanceador)
					//System.out.println("[Server] Cliente conectado");
					
					BufferedReader input = new BufferedReader (new InputStreamReader (s.getInputStream()));
					PrintWriter output = new PrintWriter (new OutputStreamWriter (s.getOutputStream()),true);
					
					String msg = input.readLine();
					System.out.println("[Server] Llego: '" + msg + "'");
					String respuesta = this.evaluar(msg);

					output.println(respuesta);
					System.out.println("[Server] Respondi: '" + respuesta + "'");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private String evaluar(String msg) {
		String respuesta;
		
		switch (msg) {
		//mensajes de los clientes
		case "CONNECT":
				this.clientes++;
				respuesta = "OK";
			break;
		case "DISCONNECT":
				this.clientes--;
				respuesta = "OK";
			break;
		
		//mensajes del balanceador
		case "CONNECTED":
				respuesta = String.valueOf(clientes);
			break;
		case "UP":
				if (!myConf.isActive()) {
					myConf.setActive(true);
				}
				respuesta = "OK";
			break;	
		case "DOWN":
			if (myConf.isActive()) {
				myConf.setActive(false);
			}
			respuesta = "OK";
		break;
		
		default:
			respuesta = "ERROR";
			break;
		}
		
		return respuesta;
	}
	
	public static void main(String[] args) {
		ServerStatus configuracion = new ServerStatus("127.0.0.1", 10002);
		Servidor sv = new Servidor(configuracion, 0);
		sv.iniciar();
	}
}
